﻿using System.ComponentModel.DataAnnotations;

namespace Ftemov.MVC.Models
{
    public class SportSectionType
    {
        public int Id { get; set; }

        [Required]
        [StringLength(25)]
        public string Name { get; set; }
    }
}