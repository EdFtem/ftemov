﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Ftemov.MVC.Models
{
    public class Complex
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public int Rating { get; set; }

        public string Description { get; set; }

        public string ComplexImgUrl { get; set; }

        [Required]
        public List<Build> Builds { get; set; }
    }
}
