﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Ftemov.MVC.Models
{
    public class Booking
    {
        public int Id { get; set; }

        [Required]
        public SportSection SportSection { get; set; }

        [Required]
        public ApplicationUser ApplicationUser { get; set; }

        [Required]
        public DateTime BookingStartingDate { get; set; }

        [Required]
        public DateTime BookingEndingDate { get; set; }

        public double Cost { get; set; }

        public int ChangingUserId { get; set; }

        public DateTime ChangedDate { get; set; }
    }
}
