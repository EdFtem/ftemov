﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Ftemov.MVC.Models
{
    public class Build
    {
        public int Id { get; set; }

        [StringLength(50)]
        public string Addres { get; set; }

        [StringLength(25)]
        public string ContactPhone { get; set; }

        [Required]
        public DateTime StartingWorkTime { get; set; }

        [Required]
        public DateTime EndingWorkTime { get; set; }

        [Required]
        public List<SportSection> SportSections { get; set; }
    }
}
