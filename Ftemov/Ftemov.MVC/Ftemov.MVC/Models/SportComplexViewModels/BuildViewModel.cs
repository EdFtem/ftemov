﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ftemov.MVC.Models.SportComplexViewModels
{
    public class BuildViewModel
    {
        public Build Build { get; set; }

        public List<SportSection> SportSection { get; set; }

        public DateTime Date { get; set; }

        public DateTime StaDateTime { get; set; }

        public DateTime EndDateTime { get; set; }

        public int SectionId { get; set; }
    }
}
