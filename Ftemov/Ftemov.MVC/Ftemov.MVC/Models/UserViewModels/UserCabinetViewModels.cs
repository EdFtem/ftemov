﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ftemov.MVC.Models.UserViewModels
{
    public class UserCabinetViewModels
    {
        public ApplicationUser ApplicationUser { get; set; }

        public List<Booking> Bookings { get; set; }
    }
}
