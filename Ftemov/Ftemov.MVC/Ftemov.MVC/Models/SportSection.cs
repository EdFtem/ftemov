﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Ftemov.MVC.Models
{
    public class SportSection
    {
        public int Id { get; set; }

        [Required]
        public SportSectionType SportSectionType { get; set; }

        public string Description { get; set; }
    }
}
