﻿(function ($) {
    $(function () {

        $('.sidenav').sidenav();
        $('.parallax').parallax();
        document.addEventListener('DOMContentLoaded', function () {
            var elems = document.querySelectorAll('.materialboxed');
            var instances = M.Materialbox.init(elems, options);
        });

        // Or with jQuery

        $(document).ready(function () {
            $('.materialboxed').materialbox();
        });

        document.addEventListener('DOMContentLoaded', function () {
            var elems = document.querySelectorAll('.collapsible');
            var instances = M.Collapsible.init(elems, options);
        });

        // Or with jQuery

        $(document).ready(function () {
            $('.collapsible').collapsible();
        });

        document.addEventListener('DOMContentLoaded', function () {
            var elems = document.querySelectorAll('.modal');
            var instances = M.Modal.init(elems, options);
        });

        // Or with jQuery

        $(document).ready(function () {
            $('.modal').modal();
        });

        document.addEventListener('DOMContentLoaded', function () {
            var elems = document.querySelectorAll('.slider');
            var instances = M.Slider.init(elems, options);
        });

        // Or with jQuery

        $(document).ready(function () {
            $('.slider').slider();
        });

        //$(document).ready(function () {
        //    $(".section-img-football").each(function () {
                
        //        $(this).attr("src", "/images/ico/football_ico.png");
        //    });
        //    $(".section-img-basketball").each(function () {

        //        $(this).attr("src", "/images/ico/basketball_ico.png");
        //    });
        //    $(".section-img-basketball").each(function () {

        //        $(this).attr("src", "/images/ico/basketball_ico.png");
        //    });
        //});

        document.addEventListener('DOMContentLoaded', function () {
            var elems = document.querySelectorAll('.timepicker');
            var instances = M.Timepicker.init(elems, options);
        });

        // Or with jQuery

        $(document).ready(function () {
            $('.timepicker').timepicker();
        });

        document.addEventListener('DOMContentLoaded', function () {
            var elems = document.querySelectorAll('.datepicker');
            var instances = M.Datepicker.init(elems, options);
        });

        // Or with jQuery

        $(document).ready(function () {
            $('.datepicker').datepicker({
                minDate: new Date(2018, 6, 21),
                maxDate: new Date(2019, 6, 21),
                defaultDate: new Date(2018, 6, 21),
                setDefaultDate : true
        });
        });
    });
})(jQuery);


