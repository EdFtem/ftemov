﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Ftemov.MVC.Data;
using Ftemov.MVC.Models.SportComplexViewModels;
using Ftemov.MVC.Models.UserViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Ftemov.MVC.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        private ApplicationDbContext _applicationDbContext;
        public UserController(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public IActionResult Index()
        {
            var user = _applicationDbContext.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
            
            var booking = _applicationDbContext.Bookings.Include(x => x.SportSection).Where(x => x.ApplicationUser.Id == user.Id).ToList();
                        
            UserCabinetViewModels userCabinetViewModels = new UserCabinetViewModels()
            {
                ApplicationUser = user,
                Bookings = booking
            };
            return View(userCabinetViewModels);
        }
    }
}