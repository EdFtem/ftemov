﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Ftemov.MVC.Data;
using Ftemov.MVC.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Ftemov.MVC.Models.SportComplexViewModels;

namespace Ftemov.MVC.Controllers
{
    public class SportComplexController : Controller
    {
        private ApplicationDbContext _applicationDbContext;
        public SportComplexController(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public IActionResult Index()
        {
            var complexes = _applicationDbContext.Complexes.ToList();
            return View(complexes);
        }

        public IActionResult ComplexProfile(int complexId)
        {
            var complex = _applicationDbContext.Complexes.Include(x => x.Builds).First(x => x.Id == complexId);
            return View(complex);
        }

        public IActionResult Build(int buildId)
        {
            var build = _applicationDbContext.Builds.Include(x => x.SportSections).First(x => x.Id == buildId);
            var sections = _applicationDbContext.SportSections.Include(x => x.SportSectionType).Where(x => build.SportSections.Select(y => y.Id).Contains(x.Id)).ToList();
            BuildViewModel buildViewModel = new BuildViewModel()
            {
                Build = build,
                SportSection = sections
            };

            return View(buildViewModel);
        }

        [HttpPost]
        public ActionResult SetBookig(BuildViewModel model)
        {
            Random r = new Random(DateTime.Now.Second);
            var user = _applicationDbContext.Users.First(x => x.UserName == User.Identity.Name);
            DateTime sTime = new DateTime(model.Date.Year, model.Date.Month, model.Date.Day, model.StaDateTime.Hour, model.StaDateTime.Minute, model.StaDateTime.Second);
            DateTime eTime = new DateTime(model.Date.Year, model.Date.Month, model.Date.Day, model.EndDateTime.Hour, model.EndDateTime.Minute, model.EndDateTime.Second);
            SportSection s = _applicationDbContext.SportSections.First(x => x.Id == model.SectionId);
            Booking booking = new Booking()
            {
                ApplicationUser = user,
                BookingStartingDate = sTime,
                BookingEndingDate = eTime,
                ChangedDate = DateTime.Now,
                Cost = r.Next(100, 5000),
                SportSection = s
            };
            _applicationDbContext.Bookings.Add(booking);
            _applicationDbContext.SaveChanges();
            return RedirectToAction("Index", "User");
        }

        public IActionResult SectionPartial()
        {
            return PartialView();
        }
    }
}