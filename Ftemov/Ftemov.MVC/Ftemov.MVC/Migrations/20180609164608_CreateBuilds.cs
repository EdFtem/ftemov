﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Ftemov.MVC.Migrations
{
    public partial class CreateBuilds : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BuildId",
                table: "SportSections",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Builds",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Addres = table.Column<string>(maxLength: 50, nullable: true),
                    ContactPhone = table.Column<string>(maxLength: 25, nullable: true),
                    EndingWorkTime = table.Column<DateTime>(nullable: false),
                    StartingWorkTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Builds", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SportSections_BuildId",
                table: "SportSections",
                column: "BuildId");

            migrationBuilder.AddForeignKey(
                name: "FK_SportSections_Builds_BuildId",
                table: "SportSections",
                column: "BuildId",
                principalTable: "Builds",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SportSections_Builds_BuildId",
                table: "SportSections");

            migrationBuilder.DropTable(
                name: "Builds");

            migrationBuilder.DropIndex(
                name: "IX_SportSections_BuildId",
                table: "SportSections");

            migrationBuilder.DropColumn(
                name: "BuildId",
                table: "SportSections");
        }
    }
}
