﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Ftemov.MVC.Migrations
{
    public partial class PopulateSectionType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("INSERT INTO dbo.SportSectionTypes (Name) VALUES('football')");
            migrationBuilder.Sql("INSERT INTO dbo.SportSectionTypes (Name) VALUES('basketball')");
            migrationBuilder.Sql("INSERT INTO dbo.SportSectionTypes (Name) VALUES('tenis')");
            migrationBuilder.Sql("INSERT INTO dbo.SportSectionTypes (Name) VALUES('volleyball')");
            migrationBuilder.Sql("INSERT INTO dbo.SportSectionTypes (Name) VALUES('ice hockey')");
            migrationBuilder.Sql("INSERT INTO dbo.SportSectionTypes (Name) VALUES('boxing')");
            migrationBuilder.Sql("INSERT INTO dbo.SportSectionTypes (Name) VALUES('rugby')");
            migrationBuilder.Sql("INSERT INTO dbo.SportSectionTypes (Name) VALUES('cricket')");
            migrationBuilder.Sql("INSERT INTO dbo.SportSectionTypes (Name) VALUES('golf')");
            migrationBuilder.Sql("INSERT INTO dbo.SportSectionTypes (Name) VALUES('badminton')");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM dbo.SportSectionTypes");
        }
    }
}
